# Proyecto final de Arquitectura de software

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción
Se ha solicitado a la empresa CMRG realizar un una aplicación de software, por parte de la productora y distribuidora Caramelos y Dulces S.A de C.V, para modernizar sus procesos internos de trabajo, buscando optimizar sus operaciones.

### Contexto del negocio

#### Antecedentes
Actualmente la distribuidora Caramelos y Dulces S.A de C.V tiene presencia comercial únicamente en la CDMX, buscando posicionarse como unos de los principales distribuidores en su ramo y expandirse a otras demarcaciones en el país, ha desarrollado un plan de reestructuración comercial, en el cual se busca sumar se al modelo de negocio de comercio electrónico, basado en transacciones de productos y servicios tanto en páginas web, como en redes sociales.

#### Fase del problema

El plan comercial incluye una reestructuración de tres etapas, la primera etapa buscar promover un manejo exacto y eficiente de sus transacciones internas, como son altas, bajas y modificaciones de sus productos.

La segunda etapa está centrada en ofrecer ventas electrónicas al mayoreo y menudeo.

La tercera etapa busca tener una plataforma digital completamente funcional, acorde a  nuestras necesidades, como son inventarios, ventas, y entregas.
Como parte de la primera etapa solicitamos lo siguiente:

#### Puntos de oportunidad
 
*	Migrar los datos actuales a una nueva base de datos.
*	Ingresar productos al sistema en cualquier horario.
*	Disponer de la información de cada producto.
*	Consulta total del inventario.
*	Consulta de historial de precios, pz, kg, etc.
*	Actualización de información de productos.
*	Bajas de productos del inventario.



